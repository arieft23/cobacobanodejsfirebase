import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          HELLO WORLD, we are from
        </p>
        <h1>
          IEBS
        </h1>
        <p className="App-intro">
          Anggota:<br></br>
          Adiguna Triputra - 1506689004 <br></br>
          Arief Tritomo - 1506689061 <br></br>
          I Putu Agastya Werdikatama - 1506735370 <br></br>
          Mega Mutiara Hirafahtia - 1506758020 <br></br>
          Muhammad Fauzan Fikri - 1506688802 <br></br>
        </p>
      </div>
    );
  }
}

export default App;
